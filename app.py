import requests
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return requests.get('https://api.binance.com/api/v3/ticker/price').content

if __name__ == '__main__':
    app.run(debug=False)
